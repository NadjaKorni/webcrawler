import time
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from crawler.CrawledArticle import CrawledArticle
import csv


class ArticleFetcher():

    def fetch(self):
        url = "http://python.beispiel.programmierenlernen.io/index.php"
        a = 2
        while url != "":
            r = requests.get(url)
            doc = BeautifulSoup(r.text, 'html.parser')
            time.sleep(1)
            print(url)

            for card in doc.select(".card"):
                emoji = card.select_one(".emoji").text
                content = card.select_one(".card-text").text
                title = card.select(".card-title span")[1].text
                image = urljoin(url, card.select_one("img").attrs["src"])
                print(emoji, title, image, content)
                yield CrawledArticle(title, emoji, content, image)

            if doc.select(".navigation .btn"):
                url = "http://python.beispiel.programmierenlernen.io/index.php?page=" + str(a)
                a += 1
            else:
                url = ""
        return
