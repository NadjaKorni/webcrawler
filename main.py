import crawler
import csv

fetcher = crawler.ArticleFetcher()

counter = 0
with open("crawler_output.csv", "w", newline='', encoding='UTF-8') as f:
    writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    for article in fetcher.fetch():
        writer.writerow([article.emoji, article.title, article.image, article.content])
        if counter == 16:
            break
        counter = counter + 1
        print(article.emoji + ": " + article.title)
